import os
from dotenv import load_dotenv

load_dotenv()

from sqlalchemy.orm import Session
from app.services.auth import get_db
from app.services.automated_bot import bot_helper_service

number_of_users = int(os.environ.get("number_of_users"))
max_posts_per_user = int(os.environ.get("max_posts_per_user"))
max_likes_per_user = int(os.environ.get("max_likes_per_user"))

if __name__ == "__main__":
    db: Session = next(get_db())
    bot_helper_service.run(
        db=db,
        number_of_users=number_of_users,
        max_posts_per_user=max_posts_per_user,
        max_likes_per_user=max_likes_per_user,
    )

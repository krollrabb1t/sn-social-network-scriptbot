import os
import uvicorn

# take environment variables from .env in dev environment
from dotenv import load_dotenv

load_dotenv(".env")
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.core.config import settings


from app.api.routes import router

app = FastAPI(title="SocialNetwork API")
app.include_router(router)


@app.get("/health")
def health():
    return "ok"


origins = [
    "http://localhost:3000",
] + os.environ.get(
    "ALLOWED_ORIGINS", ""
).split(",")

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)

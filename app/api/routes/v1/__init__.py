from fastapi import APIRouter
from app.api.routes.v1.account import router as account_router
from app.api.routes.v1.post import router as post_router
from app.api.routes.v1.analytics import router as analytics_router


router = APIRouter(prefix="/v1")
router.include_router(account_router)
router.include_router(post_router)
router.include_router(analytics_router)

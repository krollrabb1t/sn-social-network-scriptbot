from typing import List

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.crud import post_crud
from app.models import Account
from app.schemas import (
    TokenData,
    PostSchema,
    PostCreate,
    PostDelete,
    PostLikeSchema,
    PostLikeDelete,
)
from app.services import auth, save_last_action_on_success

router = APIRouter(prefix="/post")


@router.get("/all", response_model=List[PostSchema])
@save_last_action_on_success
def get_all_posts(
    *,
    db: Session = Depends(auth.get_db),
    account: Account = Depends(auth.auth_account),
    token: TokenData = Depends(auth.auth)
):
    return post_crud.list(db=db)


@router.post("/create", response_model=PostSchema)
@save_last_action_on_success
def create_post(
    *,
    db: Session = Depends(auth.get_db),
    account: Account = Depends(auth.auth_account),
    token: TokenData = Depends(auth.auth),
    post: PostCreate
):
    return post_crud.create(db=db, obj_in=post)


@router.post("/delete", response_model=bool)
@save_last_action_on_success
def delete_post(
    *,
    db: Session = Depends(auth.get_db),
    account: Account = Depends(auth.auth_account),
    token: TokenData = Depends(auth.auth),
    post: PostDelete
):
    return post_crud.remove(db=db, obj_in=post)


@router.post("/like", response_model=bool)
@save_last_action_on_success
def like_post(
    *,
    db: Session = Depends(auth.get_db),
    account: Account = Depends(auth.auth_account),
    token: TokenData = Depends(auth.auth),
    post: PostLikeSchema
):
    return post_crud.like(db=db, obj_in=post)


@router.post("/unlike", response_model=bool)
@save_last_action_on_success
def unlike_post(
    *,
    db: Session = Depends(auth.get_db),
    account: Account = Depends(auth.auth_account),
    token: TokenData = Depends(auth.auth),
    post: PostLikeDelete
):
    return post_crud.unlike(db=db, obj_id=post)

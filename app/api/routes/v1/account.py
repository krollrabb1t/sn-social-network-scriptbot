from datetime import timedelta

from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from fastapi_versioning import version

from app import crud
from app.models import Account
from app.schemas import AccountSchema
from app.schemas.account import AccountRegistrationSchema, Token, TokenData
from app.services import auth, save_last_action_on_success
from app.services.auth import (
    authenticate,
    create_access_token,
)

router = APIRouter(prefix="/account")


@router.post("/sign-up", response_model=AccountSchema, status_code=201)
@version(1, 0)
async def signup(
    *,
    db: Session = Depends(auth.get_db),
    profile_in: AccountRegistrationSchema,
):
    account = (
        db.query(Account)
        .filter(Account.email == profile_in.email.lower())
        .first()
    )
    if account:
        raise HTTPException(
            status_code=400,
            detail="The user with this email already exists in the system",
        )

    return crud.account_crud.create(db=db, obj_in=profile_in)


@router.post("/sign-in", response_model=Token)
def signin(
    *,
    db: Session = Depends(auth.get_db),
    form_data: OAuth2PasswordRequestForm = Depends(),
    remember_me: bool = False,
):
    user = authenticate(
        email=form_data.username, password=form_data.password, db=db
    )
    if not user:
        raise HTTPException(
            status_code=400, detail="Incorrect username or password"
        )
    lifetime = timedelta(days=7) if remember_me else timedelta(1)
    access_token = create_access_token(lifetime=lifetime, sub=user.id)

    crud.account_crud.set_last_login(db=db, account_id=user.id)

    return Token(access_token=access_token, token_type="bearer")


@router.get("/check-auth", response_model=TokenData)
def check_auth(*, token_data: TokenData = Depends(auth.auth)):
    return token_data


@router.get("", response_model=AccountSchema)
@save_last_action_on_success
def get_account(
    *,
    db: Session = Depends(auth.get_db),
    account: Account = Depends(auth.auth_account),
    token: TokenData = Depends(auth.auth),
):
    return crud.account_crud.get(db=db, id=account.id)


@router.get("/activity")
def get_activity(*, db: Session = Depends(auth.get_db), account_id: int):
    return crud.account_crud.get_activity(db=db, obj_id=account_id)

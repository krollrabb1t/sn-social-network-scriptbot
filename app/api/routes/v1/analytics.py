from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from app.services import auth, analytics_service
from app.services.analytics import date_from_string

router = APIRouter(prefix="/analytics")


@router.get("")
def get_analytics(
    *, db: Session = Depends(auth.get_db), date_from: str, date_to: str = None
):
    return analytics_service.get_likes_analytics(
        db, date_from_string(date_from), date_from_string(date_to)
    )

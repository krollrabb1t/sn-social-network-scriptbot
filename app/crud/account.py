from datetime import datetime
from typing import Optional

from sqlalchemy import Sequence
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models import Account
from app.models.account import AccountActivity
from app.schemas import (
    AccountRegistrationSchema,
    AccountSchema,
)
from app.services.auth import get_password_hash, create_salt


class AccountCRUD(CRUDBase[Account, AccountSchema]):
    def get(self, db: Session, id: int) -> Optional[AccountSchema]:
        return db.query(self.model).filter(self.model.id == id).first()

    def create(
        self, db: Session, *, obj_in: AccountRegistrationSchema
    ) -> AccountSchema:
        salt = create_salt()
        password_hash = get_password_hash(obj_in.password, salt)
        account_id = db.execute(Sequence("account_id_seq"))
        account_obj = Account(
            id=account_id,
            email=obj_in.email,
            first_name=obj_in.first_name,
            last_name=obj_in.last_name,
            password_hash=password_hash,
            password_salt=salt,
        )
        db.add(account_obj)
        db.commit()
        db.refresh(account_obj)

        account_activity = AccountActivity(account_id=account_obj.id)
        db.add(account_activity)
        db.commit()

        return AccountSchema(
            id=account_id,
            email=obj_in.email,
            first_name=obj_in.first_name,
            last_name=obj_in.last_name,
        )

    def remove(self, db: Session, *, obj_id: int) -> Account:
        return super().remove(db, obj_id=obj_id)

    @staticmethod
    def get_activity(db: Session, *, obj_id: int) -> AccountActivity:
        return (
            db.query(AccountActivity)
            .filter(AccountActivity.account_id == obj_id)
            .first()
        )

    @staticmethod
    def set_last_login(db: Session, *, account_id: int) -> bool:
        db.query(AccountActivity).filter(
            AccountActivity.account_id == account_id
        ).update({"last_login_datetime": datetime.now()})
        db.commit()
        return True

    @staticmethod
    def set_last_action(db: Session, *, account_id: int) -> bool:
        db.query(AccountActivity).filter(
            AccountActivity.account_id == account_id
        ).update({"last_action_datetime": datetime.now()})
        db.commit()
        return True


account_crud = AccountCRUD(Account)

from typing import List

from sqlalchemy.orm import Session, joinedload

from app.crud.base import CRUDBase
from app.models import Post, PostLike
from app.schemas import (
    PostSchema,
    PostLikeDelete,
    PostCreate,
    PostDelete,
    PostLikeSchema,
)


class PostCRUD(CRUDBase[Post, PostSchema]):
    def list(self, db: Session) -> List[Post]:
        return (
            db.query(self.model).options(
                joinedload(self.model.likes).joinedload(
                    PostLike.liked_by_account
                )
            )
        ).all()

    def create(self, db: Session, *, obj_in: PostCreate) -> PostSchema:
        new_post = Post(
            title=obj_in.title,
            body=obj_in.body,
            account_id=obj_in.account_id,
        )
        db.add(new_post)
        db.commit()
        return new_post

    def remove(self, db: Session, *, obj_in: PostDelete) -> bool:
        remove_post = (
            db.query(self.model).filter(
                self.model.id == obj_in.post_id,
                self.model.account_id == obj_in.account_id,
            )
        ).one()
        db.delete(remove_post)
        db.commit()
        return True

    @staticmethod
    def unlike(db: Session, *, obj_id: PostLikeDelete) -> bool:
        db.query(PostLike).filter(
            PostLike.post_id == obj_id.post_id,
            PostLike.account_id == obj_id.account_id,
        ).delete(synchronize_session=False)
        db.commit()
        return True

    @staticmethod
    def like(db: Session, *, obj_in: PostLikeSchema) -> bool:
        try:
            new_post_like = PostLike(
                post_id=obj_in.post_id,
                account_id=obj_in.liked_by_account.id,
                created_datetime=obj_in.created_datetime,
            )
            db.add(new_post_like)
            db.commit()
        except Exception as err:
            print(err)
            return False
        return True


post_crud = PostCRUD(Post)

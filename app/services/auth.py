import hashlib
import secrets
from datetime import datetime, timedelta
from typing import Generator, Optional

from fastapi import Depends
from fastapi.security import OAuth2PasswordBearer
from jose import jwt, JWTError
from sqlalchemy.orm import Session

from app.core.config import settings
from app.db.session import SessionLocal
from app.models import Account
from app.schemas.account import TokenData
from app.services.base import credentials_exception


def get_db() -> Generator:
    db = SessionLocal()
    db.current_user_id = None
    try:
        yield db
    finally:
        db.close()


def verify_password(password: str, salt: str, hashed_password: str) -> bool:
    hash_bin = hashlib.pbkdf2_hmac(
        "sha256", password.encode(), salt.encode(), 100000
    )
    return hash_bin.hex() == hashed_password


def get_password_hash(password: str, salt: str) -> str:
    hash_bin = hashlib.pbkdf2_hmac(
        "sha256", password.encode(), salt.encode(), 100000
    )
    return hash_bin.hex()


def create_salt() -> str:
    return secrets.token_hex(16)


def authenticate(
    *, email: str, password: str, db: Session
) -> Optional[Account]:
    account = db.query(Account).filter(Account.email == email.lower()).first()

    # user does not exist
    if not account:
        return None

    # incorrect password
    if not verify_password(
        password, account.password_salt, account.password_hash
    ):
        return None

    return account


def create_access_token(*, lifetime: timedelta, sub: int) -> str:
    payload = {}
    expire = datetime.utcnow() + lifetime
    payload["type"] = "access_token"

    # https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.3
    # The "exp" (expiration time) claim identifies the expiration time on
    # or after which the JWT MUST NOT be accepted for processing
    payload["exp"] = expire

    # The "iat" (issued at) claim identifies the time at which the
    # JWT was issued.
    payload["iat"] = datetime.utcnow()

    # The "sub" (subject) claim identifies the principal that is the
    # subject of the JWT
    payload["sub"] = str(sub)

    return jwt.encode(
        payload, settings.JWT_SECRET, algorithm=settings.ALGORITHM
    )


def validate_token(token):
    try:
        payload = jwt.decode(
            token,
            settings.JWT_SECRET,
            algorithms=[settings.ALGORITHM],
            options={"verify_aud": False},
        )
        token_data = TokenData(
            type=payload.get("type"),
            user=payload.get("sub"),
            expire=payload.get("exp"),
        )

        if token_data.user is None:
            raise credentials_exception

    # invalid token or expired token
    except JWTError:
        raise credentials_exception
    return token_data


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="api/v1/account/sign-in")


async def auth(token: str = Depends(oauth2_scheme)) -> TokenData:
    validated_token = validate_token(token)
    return validated_token


async def auth_account(
    db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)
) -> Account:
    token_data = validate_token(token)
    user = db.query(Account).filter(Account.id == token_data.user).first()
    if user is None:
        raise credentials_exception
    return user

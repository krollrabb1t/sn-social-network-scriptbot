from app import crud
from functools import wraps


def save_last_action_on_success(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        res = func(*args, **kwargs)
        if res:
            crud.account_crud.set_last_action(
                db=kwargs["db"], account_id=kwargs["account"].id
            )
        return res

    return wrapper

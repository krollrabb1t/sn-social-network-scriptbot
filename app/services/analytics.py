from datetime import datetime, date
from typing import Optional

from fastapi import HTTPException

from app.models import PostLike


def date_from_string(date_string: str) -> Optional[date]:
    if date_string:
        return datetime.strptime(date_string, "%Y-%m-%d").date()


class Analytics:
    @classmethod
    def get_likes_analytics(
        cls, db, date_from: datetime = None, date_to: datetime = None
    ):
        if not date_from:
            raise HTTPException(
                status_code=400,
                detail="date_from field is required",
            )
        filters = []
        if date_to:
            filters.append(PostLike.created_datetime <= date_to)

        return (
            db.query(PostLike)
            .filter(PostLike.created_datetime >= date_from, *filters)
            .all()
        )


analytics_service = Analytics()

import copy
import datetime
import random
import string
from typing import List
import itertools
from sqlalchemy.orm import Session

from app import crud
from app.models import Post, Account, PostLike
from app.schemas import AccountRegistrationSchema


class BotHelper:
    number_of_users = 0
    max_posts_per_user = 0
    max_likes_per_user = 0

    @staticmethod
    def _random_string(length=15):
        return "".join(
            random.choice(string.ascii_letters) for _ in range(length)
        )

    @classmethod
    def _random_email(cls):
        return f"{cls._random_string()}@{cls._random_string()}.com"

    def populate_users(self, db: Session):
        users = [
            AccountRegistrationSchema(
                email=self._random_email(),
                first_name=self._random_string(),
                last_name=self._random_string(),
                password=self._random_string(),
            )
            for _ in range(self.number_of_users)
        ]

        for user in users:
            crud.account_crud.create(db=db, obj_in=user)

    def populate_posts(self, db: Session):
        posts = list(
            itertools.chain(
                *[
                    [
                        Post(
                            title=self._random_string(20),
                            body=self._random_string(120),
                            account_id=account_id,
                        )
                        for _ in range(self.max_posts_per_user)
                    ]
                    for [account_id] in db.query(Account.id).all()
                ]
            )
        )

        db.add_all(posts)
        db.commit()

    def populate_likes(self, db: Session):
        posts = db.query(Post).all()
        accounts = db.query(Account).all()

        likes = []
        for account in accounts:
            current_posts: List[Post] = copy.copy(posts)
            random.shuffle(current_posts)
            current_likes = [
                PostLike(
                    post_id=current_posts.pop().id,
                    account_id=account.id,
                    created_datetime=datetime.datetime.now(),
                )
                for _ in range(self.max_likes_per_user)
            ]
            likes.extend(current_likes)
        db.add_all(likes)
        db.commit()

    def run(
        self,
        db: Session,
        number_of_users: int,
        max_posts_per_user: int,
        max_likes_per_user: int,
    ):
        if max_likes_per_user > max_posts_per_user * max_posts_per_user:
            raise ValueError("Too many likes set in config")
        self.number_of_users = number_of_users
        self.max_posts_per_user = max_posts_per_user
        self.max_likes_per_user = max_likes_per_user

        self.populate_users(db=db)
        self.populate_posts(db=db)
        self.populate_likes(db=db)


bot_helper_service = BotHelper()

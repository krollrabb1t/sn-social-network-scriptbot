from typing import List
from pydantic import AnyHttpUrl, BaseSettings


class Settings(BaseSettings):
    PROFILE_IMAGES_BUCKET = ""
    JWT_SECRET: str = "secret!"
    ALGORITHM: str = "HS256"

    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = []


settings = Settings()

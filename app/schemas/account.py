from typing import Optional
from fastapi_camelcase import CamelModel


class AccountRegistrationSchema(CamelModel):
    email: str
    first_name: str
    last_name: str
    password: str


class AccountSchema(CamelModel):
    id: int
    email: str
    first_name: str
    last_name: str

    class Config:
        orm_mode = True


class AccountActivitySchema(CamelModel):
    account_id: int
    last_login_datetime: Optional[str]
    last_action_datetime: Optional[str]


class Token(CamelModel):
    access_token: str
    token_type: str


class TokenData(CamelModel):
    type: str
    user: str
    expire: int


class AccountCreateSchema(CamelModel):
    id: int
    email: str
    first_name: str
    last_name: str
    password_hash: str
    password_salt: str

    class Config:
        orm_mode = True

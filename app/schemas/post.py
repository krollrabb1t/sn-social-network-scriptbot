from datetime import datetime
from typing import List

from fastapi_camelcase import CamelModel


class PostLikedByAccount(CamelModel):
    id: int
    first_name: str
    last_name: str

    class Config:
        orm_mode = True


class PostLikeSchema(CamelModel):
    post_id: int
    created_datetime: datetime
    liked_by_account: PostLikedByAccount

    class Config:
        orm_mode = True


class PostSchema(CamelModel):
    id: int
    title: str
    body: str
    account_id: int
    created_datetime: datetime
    likes: List[PostLikeSchema] = []

    class Config:
        orm_mode = True


class PostCreate(CamelModel):
    title: str
    body: str
    account_id: int

    class Config:
        orm_mode = True


class PostDelete(CamelModel):
    post_id: int
    account_id: int


class PostLikeDelete(CamelModel):
    post_id: int
    account_id: int


class PostLikeCreate(CamelModel):
    post_id: int
    account_id: int

"""unique like per post

Revision ID: a2da28dee710
Revises: 2abb323f659c
Create Date: 2023-03-30 10:28:38.696740

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "a2da28dee710"
down_revision = "2abb323f659c"
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_unique_constraint(
        "post_id_account_id_key", "postlike", ["post_id", "account_id"]
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint("post_id_account_id_key", "postlike", type_="unique")
    # ### end Alembic commands ###

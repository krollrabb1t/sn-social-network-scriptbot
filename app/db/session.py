import os

import sqlalchemy.exc
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


db_name = os.environ["DB_NAME"]
db_username = os.environ["DB_USERNAME"]
db_password = os.environ["DB_PASSWORD"]
db_host = os.environ["DB_HOST"]

connection_string = (
    f"postgresql+psycopg2://{db_username}:{db_password}@{db_host}/{db_name}"
)
print(connection_string)

engine = create_engine(
    url=connection_string,
    pool_pre_ping=True,
    pool_recycle=3600,  # this line might not be needed
    connect_args={
        "keepalives": 1,
        "keepalives_idle": 30,
        "keepalives_interval": 10,
        "keepalives_count": 5,
    },
)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def print_database_information():
    import configparser

    alembic_ini_path = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..", "..", "alembic.ini")
    )

    config = configparser.ConfigParser(os.environ)
    config.read(alembic_ini_path)

    print("Connecting to database via SQLAlchemy engine...")
    try:
        engine.connect()
        print("Connected Successfully")
    except sqlalchemy.exc.OperationalError as e:
        print(f"Failed to connect to database. Error: \n{e.orig}")
        exit(0)
    except Exception as e:
        print(e)


print_database_information()

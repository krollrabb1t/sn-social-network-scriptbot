from sqlalchemy.orm import as_declarative, declared_attr
import typing as t


class_registry: t.Dict = {}


@as_declarative(class_registry=class_registry)
class Base:
    id: t.Any
    __name__: str

    @declared_attr
    def __tablename__(self) -> str:
        return self.__name__.lower()

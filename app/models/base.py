from sqlalchemy import Column, TIMESTAMP, func


class TimestampMixin(object):
    created_datetime = Column(
        TIMESTAMP, default=func.now(), nullable=False, server_default=func.now()
    )
    updated_datetime = Column(TIMESTAMP, nullable=True)

from sqlalchemy import Column, Integer, Sequence, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship

from app.db.base import Base
from app.models.base import TimestampMixin


class Account(Base, TimestampMixin):
    id = Column(
        Integer, Sequence("account_id_seq"), primary_key=True, index=True
    )
    email = Column(String, nullable=False, unique=True)
    first_name = Column(
        String(),
        nullable=False,
    )
    last_name = Column(
        String(),
        nullable=False,
    )
    password_hash = Column(String(), nullable=False)
    password_salt = Column(String(), nullable=False)

    account_activity = relationship("AccountActivity", back_populates="account")

    posts = relationship("Post", back_populates="account")
    likes = relationship("PostLike", back_populates="liked_by_account")


class AccountActivity(Base):
    account_id = Column(
        Integer, ForeignKey("account.id"), primary_key=True, index=True
    )
    last_login_datetime = Column(DateTime, nullable=True)
    last_action_datetime = Column(DateTime, nullable=True)

    account = relationship("Account", back_populates="account_activity")

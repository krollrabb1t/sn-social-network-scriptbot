from sqlalchemy import (
    Column,
    Integer,
    Sequence,
    String,
    ForeignKey,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship
from app.db.base import Base
from app.models.base import TimestampMixin


class Post(Base, TimestampMixin):
    id = Column(Integer, Sequence("post_id_seq"), primary_key=True, index=True)
    title = Column(String, nullable=False)
    body = Column(String, nullable=False)
    account_id = Column(Integer, ForeignKey("account.id"), nullable=False)

    account = relationship("Account", back_populates="posts")
    likes = relationship(
        "PostLike", back_populates="post", cascade="all, delete"
    )


class PostLike(Base, TimestampMixin):
    __table_args__ = (
        UniqueConstraint(
            "post_id",
            "account_id",
            name="post_id_account_id_key",
        ),
    )
    id = Column(
        Integer, Sequence("postlike_id_seq"), primary_key=True, index=True
    )
    post_id = Column(Integer, ForeignKey("post.id"), index=True)
    account_id = Column(Integer, ForeignKey("account.id"), index=True)

    liked_by_account = relationship("Account", back_populates="likes")
    post = relationship("Post", back_populates="likes")

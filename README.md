# SN-social-network+scriptbot



## Getting started

 - Run `docker-compose up --build` for starting backend and database
 - Run migrations with `docker-compose run backend alembic upgrade head`
 - For running the population script, use `docker-compose run backend python automated_bot/automated_bot.py`


--- 
Frontend part can be accessed at https://gitlab.com/krollrabb1t/sn-fe-social-network